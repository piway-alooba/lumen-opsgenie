<?php
namespace Alooba\LumenOpsgenieAlert;

use GuzzleHttp\Client;

class Alert
{
    /**
     * @var string
     */
    private $serviceName;
    /**
     * @var string
     */
    private $key;

    public function __construct(string $serviceName, string $key)
    {
        $this->serviceName = $serviceName;
        $this->key = $key;
    }

    public function send(\Throwable $error)
    {
        $client = new Client([
            'base_uri' => 'https://api.eu.opsgenie.com/v2/',
        ]);
        $client->post('alerts', [
            'headers' => [
                'Authorization' => 'GenieKey ' . $this->key,
                'Content-Type' =>  'application/json',
            ],
            'json' => [
                'message' => 'Error on ' . $this->serviceName,
                'description' => $this->formatDescription($error),
                'priority' => 'P2',
                'alias' => $this->generateAlias($error),
            ],
        ]);
    }

    private function generateAlias(\Throwable $error): string
    {
        return sha1($error->getTraceAsString());
    }

    private function formatDescription(\Throwable $error): string
    {
        return $error->getMessage() . "\n" .
            $error->getTraceAsString();
    }
}